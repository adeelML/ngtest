import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { Todo, TodoStatus } from './todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html'
})
export class TodoComponent implements OnInit {
  @Input() todo: Todo;
  @Output() todoUpdate;
  @Output() todoRemoved;

  selected: boolean;
  mouseOver: boolean;
  status: typeof TodoStatus;

  newDesc: string;
  editMode: boolean;

  constructor() {
    this.todoUpdate = new EventEmitter();
    this.todoRemoved = new EventEmitter();
    this.editMode = false;
    this.mouseOver = false;
    this.status = TodoStatus;
  }

  ngOnInit(): void {
    this.selected = (this.todo.status === TodoStatus.completed);
    this.newDesc = this.todo.description;
  }

  updateTodo(): void {
    setTimeout(() => {
      this.todoUpdate.emit({
        id: this.todo.id,
        selected: this.selected,
        desc: this.newDesc
      });
    }, 0);
    this.editMode = false;
  }

  startEditMode() {
    this.editMode = true;
  }

  endEditMode() {
    this.editMode = false;
    this.updateTodo();
  }

}
