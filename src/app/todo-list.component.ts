import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Todo, TodoStatus } from './todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styles: [`
    ul { list-style: none; }
  `]
})
export class TodoListComponent {
  @Input() todos: Todo[];
  @Input() statusFilter: TodoStatus;

  @Output() todoUpdate;
  @Output() todoRemoved;

  constructor() {
    this.todoUpdate = new EventEmitter();
    this.todoRemoved = new EventEmitter();
  }
}
