import { Todo, TodoStatus } from './todo';

export const TODOS: Todo[] = [
  new Todo( {id: 130, desc: 'todo wUQwaTjduUBp', status: TodoStatus.active} ),
  new Todo( {id: 119, desc: 'todo MprvQhtqHDvRfy', status: TodoStatus.active} ),
  new Todo( {id: 183, desc: 'todo WJPffCrPpQcyb', status: TodoStatus.completed} )
];
