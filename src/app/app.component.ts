import { Component } from '@angular/core';

import { Todo, TodoStatus } from './todo';
import { TodoService } from './todo.service';
import { StatusFilterPipe } from './status-filter.pipe';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todos: Todo[];
  statusFilter: TodoStatus;
  numActive: number;
  numCompleted: number;

  constructor(private todoService: TodoService) {
    this.refreshTodos();
  }

  addTodo(desc): void {
    this.todoService.addTodo(desc);
    this.refreshTodos();
  }

  updateTodo(info): void {
      console.log('info', info);
    this.todoService.setTodoStatus(info.id, info.selected ? TodoStatus.completed : TodoStatus.active);
    this.todoService.setTodoDesc(info.id, info.desc);
    this.refreshTodos();
  }

  removeTodo(id): void {
    this.todoService.removeTodo(id);
    this.refreshTodos();
  }

  clearCompleted(): void {
    this.todoService.clearCompleted();
    this.refreshTodos();
  }

  filterList(status): void {
    this.statusFilter = status;
  }

  refreshTodos(): void {
    this.todos = this.todoService.getTodos();
    this.numActive = this.todos.filter(t => t.status === TodoStatus.active).length;
    this.numCompleted = this.todos.filter(t => t.status === TodoStatus.completed).length;
  }
}
