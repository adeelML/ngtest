import { Component, Input, Output, EventEmitter } from '@angular/core';

import { TodoStatus } from './todo';

@Component({
  selector: 'app-todo-controls',
  templateUrl: './todo-controls.component.html'
})
export class TodoControlsComponent {
  @Input() numActive: number;
  @Input() numCompleted: number;

  @Output() clearCompleted;
  @Output() filterList;

  status: (typeof TodoStatus);

  constructor() {
    this.clearCompleted = new EventEmitter();
    this.filterList = new EventEmitter();
    this.status = TodoStatus;
  }
}
