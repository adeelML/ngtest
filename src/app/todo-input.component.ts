import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styles: [`
    .todo-input { margin-bottom: 20px; }
  `]
})
export class TodoInputComponent {
  @Output() newTodo = new EventEmitter();
  newDesc: string;

  addTodo(): void {
    this.newTodo.emit(this.newDesc);
    this.newDesc = '';
  }
}
