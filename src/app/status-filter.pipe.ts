import { Pipe, PipeTransform } from '@angular/core';

import { Todo } from './todo';

@Pipe({
  name: 'filterByStatus'
})
export class StatusFilterPipe implements PipeTransform {
  transform(todos: Todo[], statusFilter) {
    if (statusFilter !== null && (typeof statusFilter) !== 'undefined') {
      return todos.filter(t => t.status === statusFilter);
    }
    return todos;
  }
}
