import { Injectable } from '@angular/core';

import { Todo, TodoStatus } from './todo';
import { TODOS } from './mock-todos';

@Injectable()
export class TodoService {
  todos: Todo[] = TODOS;
  nextId: number = 1;

  getTodos(): Todo[] {
    return this.todos;
  }

  addTodo(desc: string): void {
    if (!desc) {
      throw new Error('Invalid todo');
    }
    let opt = {
      id: this.nextId,
      desc: desc
    };
    this.todos.push(new Todo(opt));
  }

  removeTodo(id: number): void {
    let todoId = this.todos.findIndex(t => t.id === id);
    if (todoId === -1) {
      throw new Error('Invalid id');
    }
    this.todos.splice(todoId, 1);
  }

  setTodoStatus(id: number, status: TodoStatus): void {
    let target = this.todos.find(t => t.id === id);
    if (!target) {
      throw new Error('Todo not found');
    }
    target.setStatus(status);
  }

  setTodoDesc(id: number, desc: string): void {
    let target = this.todos.find(t => t.id === id);
    if (!target) {
      throw new Error('Todo not found');
    }
    target.setDesc(desc);
  }

  clearCompleted(): void {
    this.todos = this.todos.filter(t => t.status !== TodoStatus.completed);
  }
}
