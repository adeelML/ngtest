export enum TodoStatus { active, completed };

export class Todo {
  id: number;
  description: string;
  status: TodoStatus;

  constructor(opt: {
    id: number,
    desc: string,
    status?: TodoStatus
  }) {
    this.id = opt.id;
    this.description = opt.desc;
    this.status = opt.status || TodoStatus.active;
  }

  setStatus(status: TodoStatus): void {
    this.status = status;
  }

  setDesc(desc: string): void {
    this.description = desc;
  }
}
